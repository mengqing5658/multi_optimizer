import os
import re

from src.plots.bert_accuracy_ranking_from_folder import get_accuracies

dataset = 'agnews'
model = 'BERT'
folder_path = f'../../logs/multi_optimizer/{dataset}/{model}/'

files = os.listdir(folder_path)

all_best_accuracies = []

files.sort()
old_f = 'none'
for f in files:
    clean_f = f[:-6] if re.search(r'_\d$', f[:-4]) else f[:-4]
    if not clean_f.startswith(old_f):
        old_f = clean_f
        all_best_accuracies.append([])
        index = len(all_best_accuracies) - 1
    log_path = os.path.join(folder_path, f)

    try:
        epochs, opt_accuracies = get_accuracies(log_path, verbose=0)
        if len(epochs) != 9:
            raise Exception
        flat_list = opt_accuracies
        best_acc = max(flat_list)
        all_best_accuracies[index].append((f, best_acc))
    except Exception as e:
        print(log_path, e)

all_best_accuracies_mean = []
for r in all_best_accuracies:
    runs = len(r)
    if runs > 4:
        mean_acc = sum([x[1] for x in r]) / runs
        name = r[0][0][:-4]
        all_best_accuracies_mean.append((name, mean_acc, runs))

all_best_accuracies_mean.sort(key=lambda tup: tup[1], reverse=True)
for i, (n, a, r) in enumerate(all_best_accuracies_mean):
    print(f'{i})', n, f'({r} runs)', '-->', a)
