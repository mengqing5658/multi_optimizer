import matplotlib.pyplot as plt

# file_names = ['adam', 'sgd_momentum0.95_4', 'weighted_0.3_0.7_momentum0.95_5', 'weighted_0.5_0.5_momentum0.95_1']
tr_name = {
    'adam': 'ADAM',
    'sgd_momentum0.95_4': 'SGD',
    'weighted_0.3_0.7_momentum0.95_5': 'MAS: $\lambda_a=0.3$, $\lambda_s=0.7$',
    'weighted_0.5_0.5_momentum0.95_1': 'MAS: $\lambda_a=0.5$, $\lambda_s=0.5$'
}
file_names = [k for k, _ in tr_name.items()]
model = 'resnet18'
dataset = 'cifar10'
output_path = f'../../images/multi_optimizer/{dataset}/{model}/test_loss.pdf'


def main():
    fig = plt.figure()
    ax = plt.gca()

    for file_name in file_names:
        log_path = f'../../logs/multi_optimizer/{dataset}/{model}/{file_name}.log'
        print(log_path)

        epochs, opt_accuracies, opt_names = get_accuracies(log_path)

        for y, label in zip(opt_accuracies, opt_names):
            trans_file_name = tr_name[file_name]
            ax.plot(epochs, y, label=trans_file_name)
    ax.set_ylabel('Test Loss')
    ax.set_xlabel('Epoch')
    # axleg.axis('off')
    ax.legend()
    # plt.show()
    # plt.margins(0, 0)
    plt.savefig(output_path, bbox_inches='tight', pad_inches=0)


def get_accuracies(log_path, verbose=False):
    with open(log_path, "r") as f:
        logs = str(f.readlines())
    tests = logs.split('- Test Epoch')[1:]
    opt_accuracies = []
    opt_names = []
    epochs = []
    for t in tests:
        epoch = t.split('/')[0]
        epochs.append(int(epoch))
        optimizers = t.split('  - Optimizer  ')[1:]
        if verbose:
            print('Epoch:', epoch)
        for i, o in enumerate(optimizers):
            name = o.split('\\n')[0].strip()
            acc = o.split('Test Crossentropy:')[1].split(',')[0].strip()
            if verbose:
                print(' ', name, 'accuracy:', acc)
            if len(opt_accuracies) < i + 1:
                opt_accuracies.append([])
                opt_names.append(name)
            opt_accuracies[i].append(float(acc))
    return epochs, opt_accuracies, opt_names


if __name__ == '__main__':
    main()
