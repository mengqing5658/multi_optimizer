import os

from src.plots.multi_optimizer_accuracy_plot_from_file import get_accuracies

dataset = 'cifar10'
model = 'resnet18'
folder_path = f'../../logs/multi_optimizer/{dataset}/{model}/'

files = os.listdir(folder_path)

all_best_accuracies = []

for f in files:
    log_path = os.path.join(folder_path, f)

    epochs, opt_accuracies, opt_names = get_accuracies(log_path, verbose=0)
    flat_list = [item for sublist in opt_accuracies for item in sublist]
    best_acc = max(flat_list)
    all_best_accuracies.append((f, best_acc))

all_best_accuracies.sort(key=lambda tup: tup[1], reverse=True)
for i, (n, a) in enumerate(all_best_accuracies):
    print(f'{i})', n, '-->', a)
