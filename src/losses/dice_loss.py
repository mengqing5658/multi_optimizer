import torch


class JaccardLoss(torch.nn.Module):
    def __init__(self, use_cuda=True):
        super(JaccardLoss, self).__init__()
        self.use_cuda = use_cuda

    def forward(self, x, target):
        one_hot = torch.zeros(x.shape[0], x.shape[1])
        if self.use_cuda:
            one_hot = one_hot.cuda()
        one_hot_target = one_hot.scatter_(1, target.unsqueeze(dim=1), 1)
        softmax_input = torch.nn.functional.softmax(x, dim=1)
        intersection = (softmax_input * one_hot_target).sum()
        union = one_hot_target.sum() + softmax_input.sum() - intersection
        return 1 - ((intersection + 0.01) / (union + 0.01))


if __name__ == '__main__':
    target = torch.tensor([
        0,
        1
    ], dtype=torch.int64)
    pred = torch.Tensor([
        [8., 2.],
        [6.7, 7.]
    ])
    j = JaccardLoss(use_cuda=False)

    loss = j(pred, target)
    print(loss)
