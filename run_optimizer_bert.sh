#!/usr/bin/env bash

#DATASET_PATH="/media/mint/Barracuda/Datasets/cola_public/raw"
DATASET_PATH="/home/super/datasets/cola_public/raw"
DATASET_NAME='cola'
#DATASET_PATH="/media/mint/Barracuda/Datasets/AGnews"
#DATASET_PATH="/home/super/datasets/AGnews"
#DATASET_NAME='agnews'
MODEL_NAME='BERT'
RUN_NUMBER="9"
GPU="1"
BATCH_SIZE="100"
LR=0.0002
MOMENTUM=0.95
EPOCHS=50

source venv/bin/activate

for ADAM_W_RAW in 10 7 6 5 4 3 0
do
  ADAM_W="0.$ADAM_W_RAW"
  if [ $ADAM_W_RAW -eq 10 ]
  then
    ADAM_W="1.0"
  fi
  SGD_W_RAW=$(expr 10 - $ADAM_W_RAW)
  SGD_W="0.$SGD_W_RAW"
  if [ $SGD_W_RAW -eq 10 ]
  then
    SGD_W="1.0"
  fi
  echo run ADAM:$ADAM_W SGD:$SGD_W

  CUDA_VISIBLE_DEVICES="$GPU" python3 src/bert_train.py --path="$DATASET_PATH" --dataset="${DATASET_NAME}" --lr="$LR" --batch-size="$BATCH_SIZE" \
        --momentum="$MOMENTUM" --adam-w="$ADAM_W" --sgd-w="$SGD_W" --gpu="$GPU" --max-epoch="$EPOCHS" --seed="$RUN_NUMBER" \
        > "logs/multi_optimizer/${DATASET_NAME}/${MODEL_NAME}/w_${ADAM_W}_${SGD_W}_mom_${MOMENTUM}_lr_${LR}_${RUN_NUMBER}.log"
done
